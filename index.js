
// https://nodejs.org/api/http.html
// https://davidwalsh.name/nodejs-http-request

const http = require('http');
const fs = require('fs');
const { URL, URLSearchParams } = require('url');
const cookiet = require('./cookiet')

const apiUrl = 'http://www.imdb.com/xml/find?json=1&nr=1&nm=on';

function findNames(who, callback) {
    const url = new URL(apiUrl);
    url.searchParams.set('q', who);
    http.get(url, (res) => {
        if (res.statusCode === 200) {
            let rawData = '';
            res.setEncoding('utf8');
            res.on('data', (chunk) => rawData += chunk);
            res.on('end', () => {
                try {
                    const json = JSON.parse(rawData);
                    //console.log(json);
                    callback(null, json);
                } catch (e) {
                    callback(new Error(e.message), null);
                }
            });
        }
        else {
            const err = new Error('Yawa don gas o.');
            res.resume();
            callback(err, null);
        }
    });
}

function showError(err, res) {
    res.statusCode = 500;
    res.setHeader('Content-type', 'text/plain');
    res.setHeader('Content-length', err.message.length);
    res.write(err.message);
}

function readFile(filename, callback) {
    fs.readFile(filename, 'utf8', (err, content) => {
        if (err) {
            callback(err, null);
        } else {
            callback(null, content);
        }
    });
}

function index(req, res) {
    //console.log(Object.keys(req));
    //console.log(req.headers);
    const cookies = cookiet.Cookie.getCookies(req);
    console.log(cookies);
    console.log(cookies['fizz']);
    console.log('Secret exists:', !!cookies['secret']);
    let aCookie = cookies['supersecret'];
    if (aCookie) {
        aCookie.remove(res);
    } else {
        aCookie = new cookiet.Cookie('supersecret', 'Squirrell');
        aCookie.expires = 30;
        aCookie.httpOnly = true;
        aCookie.set(res);
    }
    readFile('./index.html', (err, content) => {
        if (err) {
            showError(err, res);
        } else {
            res.statusCode = 200;
            res.setHeader('Content-type', 'text/html');
            res.setHeader('Content-length', content.length);
            res.write(content);
        }
    });
}

function greet(req, res) {
    req.on('data', (bytes) => {
        const params = new URLSearchParams(bytes.toString('utf8'));
        findNames(params.get('who'), (err, json) => {
            if (err) {
                showError(err, res);
            } else {
                let popular = ''
                json.name_popular.forEach((item, i) => {
                    popular += `<dl>
                    <dt>Name</dt>
                    <dd>${item.name}</dd>
                    <dt>Description</dt>
                    <dd>${item.description}</dd>
                    </dl>`
                })
                readFile('./greet.html', (err, content) => {
                    if (err) {
                        showError(err, res);
                    } else {
                        const html = content.replace(/\$results/, popular);
                        res.statusCode = 200;
                        res.setHeader('Content-type', 'text/html');
                        res.setHeader('Content-length', html.length);
                        res.write(html);
                    }
                });
            }
        });
    });
}

function notFound(req, res) {
    res.statusCode = 404;
    res.setHeader('Content-length', 10);
    res.write('Not found.');
}

const server = http.createServer((req, res) => {
    //console.log(Object.keys(req));
    switch (req.url) {
        case '/':
            index(req, res);
            break;
        case '/greet':
            greet(req, res);
            break;
        default:
            notFound(req, res);
            break;
    }
});

server.listen(4444);
